#!/bin/zsh

src=~/Library/Application\ Support/Code/User
dest=User
file=settings.json

move_file() {
  p="$dest/$file"
  if [[ -f $p ]]; then
    echo "remove $p"
    rm $p
  fi
  
  from="$src/$file"
  to="$dest/$file"
  
  echo "copying $from => $to" 
  cp $from $to
  cp "$src/$file" "$dest/$file"
}

move_file

file="keybindings.json"
move_file


