# Windows "dotfiles"

manage windows configuration files (mostly vscode settings)

## VSCode settings:

- repository is the source of truth

copy repository settings to local machine

    windows: "apply vscode settings to machine.bat"
    
    mac: TOOD

copy local machine changes to update repository

    windows: "copy local vscode settings to repository.bat"

    mac: TODO

After copying to repository, confirm changes and commit/push to origin


### Development on settings

Because the keybindings.json cannot have comments while using vscode builtin keybinding editor, you can:

- rename keybindings.json -> keybindings1.json
- add new keybindings
- copy new keybindings from newly created keybindings.json to keybindings1.json
- remove keybindings.json and revert keybindings1.json by removing the 1
- check functionality
- copy over to repository, commit and push


## settings directory

windows

    C:\Users\travis\AppData\Roaming\Code\User\settings.json

mac

   TODO